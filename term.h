#ifndef __TERM_H__
#define __TERM_H__

#include <stdlib.h> /* size_t */

extern unsigned short term_rows, term_cols;

extern void term_resize(); /* To be implemented externally. */
extern void term_write(const char *str, size_t len);
extern void term_print(const char *str);
extern void term_printf(const char *fmt, ...);
extern void term_move(unsigned short row, unsigned short col);
extern void term_color(unsigned char a, unsigned char fg, unsigned char bg);
extern void term_sync();
extern void term_deinit();
extern void term_init();

#endif /* __TERM_H__ */
