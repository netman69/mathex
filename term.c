#include <termios.h> /* tcgetattr(), tcsetattr() */
#include <unistd.h> /* write(), fsync(), tcgetattr(), tcsetattr(), STDOUT_FILENO */
#include <string.h> /* memcpy(), strlen() */
#include <stdlib.h> /* atexit(), exit(), EXIT_FAILURE, size_t */
#include <sys/ioctl.h> /* ioctl() */
#include <signal.h> /* signal() */
#include <stdio.h> /* snprintf() */
#include <stdarg.h> /* va_list, va_start(), va_end() */

unsigned short term_rows, term_cols;

static char buf[4096];
static struct termios tios, tios_orig;
static int initialized = 0;

extern void term_resize();

static void term_sigwinch(int signal) {
	struct winsize sz;
	ioctl(0, TIOCGWINSZ, &sz);
	term_rows = sz.ws_row;
	term_cols = sz.ws_col;
	term_resize();
}

static void term_sigcont(int signal) {
	tcsetattr(STDOUT_FILENO, TCSANOW, &tios);
	term_sigwinch(signal);
}

static void term_sigexit(int signal) {
	exit(EXIT_FAILURE);
}

void term_write(const char *str, size_t len) {
	write(STDOUT_FILENO, str, len);
}

void term_print(const char *str) {
	write(STDOUT_FILENO, str, strlen(str));
}

void term_printf(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);
	term_print(buf);
}

void term_move(unsigned short row, unsigned short col) {
	term_printf("\e[%d;%dH", row, col);
}

void term_color(unsigned char a, unsigned char fg, unsigned char bg) {
	/* Attributes:
	 *  0 None
	 *  1 Bright
	 *  2 Dim
	 *  4 Underscore
	 *  5 Blink
	 *  7 Reverse
	 *  8 Hidden
	 * Colors:
	 *  0 Black
	 *  1 Red
	 *  2 Green
	 *  3 Yellow
	 *  4 Blue
	 *  5 Magenta
	 *  6 Cyan
	 *  7 White
	 */
	term_printf("\e[%u;%u;%um", a, fg + 30, bg + 40);
}

void term_sync() {
	fsync(STDOUT_FILENO);
}

void term_deinit() {
	if (initialized == 0)
		return;
	tcsetattr(STDOUT_FILENO, TCSANOW, &tios_orig);
	term_print("\e[0m\e[2J\e[1;1H\e[r\e[?1049l"); /* Clear screen + restore normal mode. */
	/* The reason we clear the screen is because maybe not all terminals support "private mode".
	 * Earlier we had "\e[0m\e[2J\e[1;1H\ec" but \ec (reset terminal state) interferes with restoring from private mode.
	 * "\e[r" is needed then to restore the scrolling window stuff (DECSTBM).
	 */
	term_sync();
	initialized = 0;
}

void term_init() { // TODO Fail if not in interactive mode etc, check error handling everywhere in this file.
	if (initialized != 0)
		return;
	term_print("\e[?1049h"); /* Turn terminal to "private mode". */
	term_sync();
	/* Do termios magic to turn off echoing etc. */
	tcgetattr(STDOUT_FILENO, &tios);
	memcpy(&tios_orig, &tios, sizeof(tios_orig));
	cfmakeraw(&tios);
	tios.c_iflag &= ~IGNBRK;
	tios.c_iflag |= BRKINT;
	tios.c_lflag |= ISIG;
	tcsetattr(STDOUT_FILENO, TCSANOW, &tios);
	atexit(term_deinit);
	signal(SIGWINCH, term_sigwinch);
	signal(SIGCONT, term_sigcont);
	signal(SIGTERM, term_sigexit);
	signal(SIGINT, term_sigexit);
	signal(SIGHUP, term_sigexit);
	initialized = 1;
	term_sigwinch(0);
}
